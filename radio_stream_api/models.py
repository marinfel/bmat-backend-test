from django.db import models


class Channel(models.Model):

    name = models.CharField(null=False, blank=False,
                            unique=True, max_length=32)


class Performer(models.Model):

    name = models.CharField(null=False, blank=False,
                            unique=True, max_length=32)


class Song(models.Model):

    title = models.CharField(max_length=32)
    performer = models.ForeignKey(Performer, related_name='songs',
                                  on_delete=models.CASCADE)

    class Meta:
        unique_together = (("title", "performer"),)


class Play(models.Model):

    song = models.ForeignKey(Song, related_name='plays',
                             on_delete=models.PROTECT)
    channel = models.ForeignKey(Channel, related_name='plays',
                                on_delete=models.CASCADE)
    start = models.DateTimeField()
    end = models.DateTimeField()
