from datetime import datetime
from dateutil.relativedelta import relativedelta

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.utils import json

from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Count, Case, When
from django.db.utils import IntegrityError

from . import serializers
from . import models


def generate_top(plays, start, limit):
    """
    Generates the top dictionary.
    Plays are all plays between start -7 days and start + 7 days.
    The function partitions the plays queryset with start date
    and generates the data dictionary to be passed to the serializer.

    TODO: Cache function result, as for many play records
    it might be slow to compute.
    """
    week_top_plays = plays.filter(
        start__gte=start.strftime('%Y-%m-%d'),
    )
    week_top = week_top_plays.values('song').annotate(
        plays=Count('song')
    ).order_by('-plays')[:limit]
    top_songs_list = [pl['song'] for pl in week_top]
    # allows to store top songs with the same order as top plays
    songs_order = Case(*[When(pk=pk, then=pos)
                         for pos, pk in enumerate(top_songs_list)])
    top_songs = models.Song.objects.filter(
        pk__in=top_songs_list
    ).order_by(songs_order)

    previous_week_top_plays = plays.exclude(
        pk__in=week_top_plays
    )
    previous_week_top = previous_week_top_plays.values('song').annotate(
        plays=Count('song')
    ).order_by('-plays')[:limit]

    for ind_play, play in enumerate(week_top):
        for ind_prev_play, prev_play in enumerate(previous_week_top):
            if play['song'] == prev_play['song']:
                play['previous_plays'] = prev_play['plays']
                play['previous_rank'] = ind_prev_play
                break
        else:
            play['previous_plays'] = 0
            play['previous_rank'] = None
        play['rank'] = ind_play
        play['title'] = top_songs[ind_play].title
        play['performer'] = top_songs[ind_play].performer
    return week_top


@api_view(['GET'])
def channel_plays(request):
    """
    Responds with the list of plays in channel_name,
    between start and end dates.
    """
    channel_name = request.query_params.get('channel', False)
    start = request.query_params.get('start', False)
    end = request.query_params.get('end', False)
    try:
        channel = models.Channel.objects.get(name=channel_name)
        plays = channel.plays.filter(
            start__gte=start,
            end__lte=end
        )
    except (ValueError, ObjectDoesNotExist,) as exception:
        return Response({'errors': [str(exception)]})
    serializer = serializers.ChannelPlaySerializer(plays, many=True)
    return Response({'result': serializer.data})


@api_view(['GET'])
def song_plays(request):
    """
    Responds with the list of plays for song
    with title and performer passed as query parameters,
    between start and end date.
    """
    title = request.query_params.get('title')
    performer = request.query_params.get('performer')
    start = request.query_params.get('start')
    end = request.query_params.get('end')
    try:
        plays = models.Play.objects.filter(
            song__title=title,
            song__performer__name=performer,
            start__gte=start,
            end__lte=end
        )
    except ValueError as exception:
        return Response({'errors': [str(exception)]})
    serializer = serializers.SongPlaySerializer(plays, many=True)
    return Response({'result': serializer.data})


@api_view(['GET'])
def get_top(request):
    """
    Responds with the top of the week after the start date passed
    in query parameters, computed only on channels passed.
    """
    channel_names = json.loads(request.query_params.get('channels'))
    limit = int(request.query_params.get('limit', 40))
    start = request.query_params.get('start', False)
    start_dt = datetime.strptime(start, '%Y-%m-%dT%H:%M:%S')
    new_start = start_dt + relativedelta(days=-7)
    end = start_dt + relativedelta(days=+7)
    week_top = {}
    try:
        plays = models.Play.objects.filter(
            channel__name__in=channel_names,
            start__gte=new_start.strftime('%Y-%m-%d'),
            end__lt=end.strftime('%Y-%m-%d')
        )
        week_top = generate_top(plays, start_dt, limit)
    except ValueError as exception:
        # ValueError is caught when the parameters
        # don't fit the required format
        return Response({'errors': [str(exception)]})
    serializer = serializers.TopPlaySerializer(week_top, many=True)
    return Response({'result': serializer.data})


@api_view(['POST'])
def add_channel(request):
    """
    Handles channel POSTing
    """
    serializer = serializers.ChannelSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response({'result': serializer.data,
                     'errors': list(serializer.errors.items())})


@api_view(['POST'])
def add_performer(request):
    """
    Handles performer POSTing
    """
    serializer = serializers.PerformerSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response({'result': serializer.data,
                     'errors': list(serializer.errors.items())})


@api_view(['POST'])
def add_song(request):
    """
    Handles song POSTing.
    if the performer doesn't exist,
    it is created before creating the song.
    """
    try:
        performer, _ = models.Performer.objects.get_or_create(
            name=request.data.get('performer')
        )
    except IntegrityError as exception:
        # Caught when there is no performer name supplied
        return Response({'errors': [str(exception)]})
    data = request.data.copy()
    data['performer'] = performer.id
    serializer = serializers.SongSerializer(data=data)
    if serializer.is_valid():
        serializer.save()
    return Response({'result': serializer.data,
                     'errors': list(serializer.errors.items())})


@api_view(['POST'])
def add_song_play(request):
    """
    Handles play POSTing
    """
    data = request.data.copy()
    try:
        song = models.Song.objects.get(
            title=data.get('title'),
            performer__name=data.get('performer')
        )
        channel = models.Channel.objects.get(
            name=data.get('channel'),
        )
    except ObjectDoesNotExist as exception:
        return Response({'errors': [str(exception)]})
    data["song"] = song.id
    data["channel"] = channel.id
    serializer = serializers.PlaySerializer(data=data)
    if serializer.is_valid():
        serializer.save()
    return Response({'result': serializer.data,
                     'errors': list(serializer.errors.items())})
