from django.apps import AppConfig


class RadioStreamApiConfig(AppConfig):
    name = 'radio_stream_api'
