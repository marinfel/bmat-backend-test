from rest_framework.test import APIClient, APITestCase
import dateutil.parser
import datetime
import collections
import copy
import json


# performer, title, length (in seconds)
song1 = ("Performer1", "Song1", 600)
song2 = ("Performer2", "Song2", 180)
song3 = ("Pêrformer3", "Söng3", 180)

# channel -> plays
plays = {
    'Channel1': {
        # year, month, day, hour, minute, second
        (2014, 1, 1, 1, 0, 0): song1,
        (2014, 1, 1, 2, 0, 0): song2,
        (2014, 1, 1, 3, 0, 0): song3,
        (2014, 1, 1, 4, 0, 0): song1,
        (2014, 1, 8, 1, 0, 0): song3,
        (2014, 1, 9, 1, 0, 0): song3,
        (2014, 1, 10, 1, 0, 0): song3,
        (2014, 1, 11, 1, 0, 0): song2},
    'Channel2': {
        (2014, 1, 1, 1, 0, 0): song2,
        (2014, 1, 1, 2, 0, 0): song2,
        (2014, 1, 1, 3, 0, 0): song2,
        (2014, 1, 1, 4, 0, 0): song2,
        (2014, 1, 8, 1, 0, 0): song1,
        (2014, 1, 9, 1, 0, 0): song1}
}


def decode_date(d):
    """
    The server sent us a date in the ISO format. Convert it to a datetime
    python object.
    """
    return dateutil.parser.parse(d)


def get_song_plays(song):
    """
    Filter the play dictionary for this particular song.
    """
    song_plays = copy.deepcopy(plays)
    for channel in plays.keys():
        banned = [date for date in song_plays[channel].keys() if
                  song_plays[channel][date] != song]
        for b in banned:
            del song_plays[channel][b]
        if not song_plays[channel]:
            del song_plays[channel]
    return song_plays


def get_top(start, channels):
    """
    Convert the play dict into a list comparable to what the server returns
    (see check_top).
    """
    pc = collections.defaultdict(int)
    previous_pc = collections.defaultdict(int)
    end = start + datetime.timedelta(days=7)
    previous_start = start - datetime.timedelta(days=7)
    for channel in plays.keys():
        if channel not in channels:
            continue
        for date, (performer, title, length) in plays[channel].items():
            date = datetime.datetime(*date)
            if start <= date < end:
                pc[(performer, title)] += 1
            elif previous_start <= date < start:
                previous_pc[(performer, title)] += 1
    res = []
    previous_pos = sorted(previous_pc.keys(), key=lambda s: -previous_pc[s])
    for song in pc.keys():
        try:
            prev = previous_pos.index(song)
        except ValueError:
            prev = None
        res.append(
            {'performer': song[0], 'title': song[1],
             'plays': pc[song], 'previous_plays': previous_pc[song],
             'previous_rank': prev})
    res = sorted(res, key=lambda x: -x['plays'])
    for i, r in enumerate(res):
        res[i]['rank'] = i
    return res


class TestAPI(APITestCase):

    @classmethod
    def setUpTestData(cls):
        """
        For each play listed above, first add the necessary metadata:
        channels, performers, songs, then the actual plays.
        Note that the metadata is posted more than once:
        if the data already exists on the server, it shouldn't be
        added again.
        """
        cls.client = APIClient()
        for channel in plays.keys():
            cls.client.post(
                '/add_channel', {"name": channel}
            )
            for start, (performer, title, length) in plays[channel].items():
                cls.client.post(
                    "/add_performer", {"name": performer},
                )
                cls.client.post(
                    "/add_song",
                    {"performer": performer,
                     "title": title},
                )
                start = datetime.datetime(*start)
                end = start + datetime.timedelta(seconds=length)
                cls.client.post(
                    "/add_play",
                    {"performer": performer,
                     "title": title,
                     "channel": channel,
                     "start": start.isoformat(), "end": end.isoformat()},
                )

    def test_channel_plays(self):
        """
        Get the plays for the two channels separately.

        The server should return something like this:

        {result: [
         {'performer': 'Performer1', 'title': 'Song1',
          'start': '2014-01-10T01:00:00',
          'end': '2014-01-10T01:03:00'],
         {'performer': 'Performer2', 'title': 'Song2',
          'start': '2014-01-01T03:00:00',
           'end': '2014-01-01T03:03:00'},...], code: 0}
        """
        for channel in plays.keys():
            chan_plays = {}
            res = self.client.get('/get_channel_plays', {
                "channel": channel,
                "start": datetime.datetime(2013, 1, 1).isoformat(),
                "end": datetime.datetime(2015, 1, 1).isoformat()
            })
            res = eval(res.content)
            self.assertEqual(res['code'], 0)
            channel_plays = res['result']
            for data in channel_plays:
                performer = data['performer']
                title = data['title']
                start = decode_date(data['start'])
                end = decode_date(data['end'])
                chan_plays[
                    (start.year, start.month, start.day,
                     start.hour, start.minute, start.second)] = (
                         performer, title, (end - start).total_seconds())
            self.assertEqual(chan_plays, plays[channel])

    def test_song_plays(self):
        """
        Check the plays for one particular song.
        Here the results should look like this:

        {result: [
         {'channel': 'channel1', 'start': '2014-01-10T01:00:00',
          'end': '2014-01-10T01:03:00'},
         {'channel': 'channel2', 'start': '2014-01-01T02:00:00',
          'end': '2014-01-01T02:03:00'}, ...], code: 0}
        """
        song3_res = self.client.get('/get_song_plays', {
            "performer": song3[0].encode('utf8'),
            "title": song3[1].encode('utf8'),
            "start": datetime.datetime(2013, 1, 1).isoformat(),
            "end": datetime.datetime(2015, 1, 1).isoformat()
        })
        song3_res = eval(song3_res.content)
        self.assertEqual(song3_res['code'], 0)
        song3_plays = song3_res['result']
        # adapt what we got from the server to compare it to the dictionary of
        # plays defined above. We want:
        # {channel: {date: song}}
        test_plays = {}
        for data in song3_plays:
            channel = data['channel']
            start = decode_date(data['start'])
            end = decode_date(data['end'])
            if channel not in test_plays:
                test_plays[channel] = {}
            test_plays[channel][
                (start.year, start.month, start.day, start.hour,
                 start.minute, start.second)] = (song3[0],
                                                 song3[1],
                                                 (end - start).seconds)
        self.assertEqual(test_plays, get_song_plays(song3))

    def test_top(self):
        """
        Here we expect a list of [performer, song, plays, previous plays,
        previous rank]. Previous ranks starts at 0.
        If the song was not in the list for the past,
        the previous rank should be null.

        {result: [
         {'performer': 'Performer1', 'title': 'Song1', 'rank': 0,
          'previous_rank': 2, 'plays': 1, 'previous_plays': 2},...],
         'code': 0}
        """
        res = self.client.get('/get_top', {
            "channels": json.dumps(list(plays.keys())),
            "start": datetime.datetime(2014, 1, 8).isoformat(),
            "limit": 10
        })
        res = eval(res.content)
        self.assertEqual(res['code'], 0)
        top = res['result']
        self.assertEqual(top, get_top(datetime.datetime(2014, 1, 8),
                                      plays.keys()))

    def test_post_song_with_wrong_value(self):
        """
        Post a song with wrong parameters:
        - No performer is passed
        - No title is passed
        Both cases should return code -1 and error strings
        """
        res = self.client.post('/add_song', {
            "title": "Test song",
        })
        self.assertEqual(res.data['code'], -1)
        self.assertEqual(json.loads(res.data['errors']), [
            'NOT NULL constraint failed: radio_stream_api_performer.name'
        ])
        res = self.client.post('/add_song', {
            "performer": "Test performer"
        })
        self.assertEqual(res.data['code'], -1)
        self.assertEqual(json.loads(res.data['errors']),
                         [["title", ["This field is required."]]])

    def test_post_play_with_wrong_value(self):
        """
        Post a play with wrong parameters:
        - No song matching query parameters (Song1, Performer2)
        - No channel provided
        - Wrong end or start date
        All cases should return code -1 and errors with response
        """
        res = self.client.post('/add_play', {
            "title": "Song1",
            "performer": "Performer2",
            "start": '2018-01-01T12:00:00',
            "channel": "Channel1",
            "end": '2018-01-01T12:04:00'
        })
        self.assertEqual(res.data['code'], -1)
        self.assertEqual(json.loads(res.data['errors']),
                         ['Song matching query does not exist.'])

        res = self.client.post('/add_play', {
           "title": "Song1",
           "performer": "Performer1",
           "start": '2018-01-01T12:00:00',
           "end": '2018-01-01T12:04:00'
        })
        self.assertEqual(res.data['code'], -1)
        self.assertEqual(json.loads(res.data['errors']),
                         ['Channel matching query does not exist.'])

        res = self.client.post('/add_play', {
            "title": "Song1",
            "performer": "Performer1",
            "channel": "Channel1",
            "start": '2018-01-01T12:00:00'
        })
        self.assertEqual(res.data['code'], -1)
        self.assertEqual(json.loads(res.data['errors']),
                         [["end", ["This field is required."]]])
