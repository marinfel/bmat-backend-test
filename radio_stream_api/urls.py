from django.urls import path
from . import views


urlpatterns = [
    path('get_channel_plays', views.channel_plays, name='channel_plays'),
    path('get_song_plays', views.song_plays, name='song_plays'),
    path('get_top', views.get_top, name='get_top'),
    path('add_channel', views.add_channel, name='add_channel'),
    path('add_song', views.add_song, name='add_song'),
    path('add_play', views.add_song_play, name='add_song_play'),
    path('add_performer', views.add_performer, name='add_performer'),
]
