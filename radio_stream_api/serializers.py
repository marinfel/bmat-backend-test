from copy import copy
from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator
from .models import Channel, Performer, Song, Play


class ChannelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Channel
        fields = ('name',)
        extra_kwargs = {
            'name': {
                'validators': [],
            }
        }

    def create(self, validated_data):
        channel, _ = Channel.objects.get_or_create(
            name=validated_data.get('name'),
            defaults={'name': validated_data.get('name')}
        )
        return channel


class PerformerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Performer
        fields = ('name',)
        extra_kwargs = {
            'name': {
                'validators': [],
            }
        }

    def create(self, validated_data):
        performer, _ = Performer.objects.get_or_create(
            name=validated_data.get('name'),
            defaults={'name': validated_data.get('name')}
        )
        return performer


class SongSerializer(serializers.ModelSerializer):

    class Meta:
        model = Song
        fields = ('title', 'performer',)

    def run_validators(self, value):
        """
        This is to bypass the UniqueTogetherValidator,
        that was applied even with the same meta parameters
        as previous classes.
        """
        for validator in copy(self.validators):
            if isinstance(validator, UniqueTogetherValidator):
                self.validators.remove(validator)
        super(SongSerializer, self).run_validators(value)

    def to_representation(self, obj):
        data = super(SongSerializer, self).to_representation(obj)
        performer = Performer.objects.get(pk=data['performer'])
        data['performer'] = performer.name
        return data

    def create(self, validated_data):
        song, _ = Song.objects.get_or_create(
            title=validated_data.get('title'),
            performer=validated_data['performer'],
            defaults=validated_data)
        return song


class PlaySerializer(serializers.ModelSerializer):

    class Meta:
        model = Play
        fields = ('song', 'channel', 'end', 'start',)


class ChannelPlaySerializer(serializers.ModelSerializer):

    performer = serializers.StringRelatedField(source='song.performer.name')
    title = serializers.StringRelatedField(source='song.title')

    class Meta:
        model = Play
        fields = ('performer', 'title', 'end', 'start',)


class SongPlaySerializer(serializers.ModelSerializer):

    channel = serializers.StringRelatedField(source='channel.name')

    class Meta:
        model = Play
        fields = ('channel', 'end', 'start',)


class TopPlaySerializer(serializers.Serializer):

    title = serializers.CharField()
    performer = serializers.CharField(source='performer.name')
    plays = serializers.IntegerField()
    previous_plays = serializers.IntegerField()
    rank = serializers.IntegerField()
    previous_rank = serializers.IntegerField()
