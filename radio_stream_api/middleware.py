import json

from rest_framework import status
from rest_framework.response import Response


class GenerateResponseMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return self.process_response(request, response)

    def process_response(self, request, response):
        """
        This method handles the response formating
        in order to make it fit the project requirements:
        {'result': [...], 'code': 0, 'errors': [...]}
        """
        if not isinstance(response, Response):
            # If it is an HttpResponse and not a DRF response
            return response
        errors = response.data.get('errors', [])
        result = response.data.get('result', [])
        status_code = status.HTTP_200_OK
        if request.method == 'POST':
            status_code = status.HTTP_400_BAD_REQUEST if errors else \
                          status.HTTP_201_CREATED
        response.data.update({'result': result,
                              'code': errors and -1 or 0
                              })
        if errors:
            response.data.update({
                'errors': json.dumps(errors)
            })
        response.status_code = status_code
        response._is_rendered = False
        response.render()
        return response
