from django.conf.urls import url
from django.conf.urls import include

import radio_stream_api.urls


urlpatterns = (
    url(r'^', include(radio_stream_api.urls)),)
