# Software engineer test

This repository contains my work for the technical test I was offered to work on.
The purpose was to implement a REST API to store and access data from radio streams.

To do it, I chose to use Django and Django Rest Framework.


## Installation steps

This project requires Python 3.6 installed to be run without docker.
To use docker to run it, you will also need Docker and docker-compose installed.

### Local use

##### First install MySQL and mysqlclient:

`sudo apt-get install mysql-server
`
`sudo apt-get install libmysqlclient-dev
`

##### Run mysql:

`mysql -u root -p
`

##### And create the database:

`CREATE DATABASE radio_streams_data;
`

##### Then go into the project folder and install all Python requirements:

`pip install -r requirements.txt
`

##### Run the following:

`python manage.py migrate
`
`python manage.py runserver
`

From there, one can use the provided test script to check the output (the port to use is 8000).
I also integrated them in an APITestCase to make it easer and avoid populating the database with test data.
To run tests this way :

`python manage.py test
`

### Run the application with docker

##### Go into the project folder and run:

`docker-compose up
`

This will run the initial migration, run unit tests and finally run the server.

## Questions

I chose to use MySQL for the database, as it is a well-documented and widely used relational database
system.

##### Design choices

I chose to use function based views: according to me, there was no need to use class based views
and function based views made it easier to manage routing so that it fits the specifications.
Although this routing could have been obtained with CBV, it would have exposed unspecified entrypoints.

To format the response, I implemented a custom middleware, that is executed on top of the middleware stack.
You can find it in the middleware.py file. In the process_response method, I chose to only process Response objects.
HttpResponse objects will be likely to be uncaught errors in views (and it shouldn't happen).

Regarding the serializers, I chose to bypass the Unique validator for unique fields: that allows one
to implement a custom behaviour when trying to post a dupplicate. Here, it simply checks the existence of
the data taking into account the query parameters, and create the object only if no matching record has been found
in the database.

##### What should be improved before production ?

Of course, the DEBUG mode would have to be disabled. Ideally, the project should implement several settings files,
to adjust to multiple environments.

I would also implement authentication to secure the access to the API.

Finally, the top generation should be cached as it might get slow with a bigger volume of plays.

##### Behaviour with millions of songs and thousands of channels

At this stage, the application would probably be very slow to respond and the database would not be able to handle so many queries per second.

To improve the project scalability, I would use Redis as an in-memory database to reduce time consumption of database queries and .
This Redis database would be flushed into the MySQL / PostgreSQL database periodically. Loadbalancing, to dispach request among
multiple applicative servers and avoid saturation, should also be implemented to handle such a volume of incoming requests.