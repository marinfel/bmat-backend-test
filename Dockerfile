FROM python:3.6
ENV PYTHONUNBUFFERED 1
RUN mkdir /opt/bmat
WORKDIR /opt/bmat
COPY . /opt/bmat
RUN pip install -r requirements.txt
RUN apt-get install


