django>=2.1.7
djangorestframework>=3.9.1
python-dateutil==2.7.3
six==1.11.0
mysqlclient>=1.3.12
